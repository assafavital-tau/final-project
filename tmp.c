///*******************************************************************************
//Copyright 2016 Microchip Technology Inc. (www.microchip.com)
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//*******************************************************************************/
//
//#include <stdio.h>
//#include <stdlib.h>
//#include <stddef.h>
//#include <stdbool.h>
//
//#include "bsp/adc.h"
//#include "bsp/lcd.h"
//#include "bsp/timer_1ms.h"
//#include "bsp/buttons.h"
//#include "bsp/leds.h"
//#include "bsp/rtcc.h"
// 
//#include "io_mapping.h"
//
//#include "mcc_generated_files/mcc.h"
//#include "mcc_generated_files/i2c1.h"
//
//// *****************************************************************************
//// *****************************************************************************
//// Section: File Scope Variables and Functions
//// *****************************************************************************
//// *****************************************************************************
//extern void SYS_Initialize ( void ) ;
//static void TimerEventHandler( void );
//
//static RTCC_DATETIME time;
//
//// *****************************************************************************
//// *****************************************************************************
//// Section: Main Entry Point
//// *****************************************************************************
//// *****************************************************************************
//int main ( void )
//{
//    float avg_adc = 0;
//    int j, i;
//    uint8_t pdata_w[2], pdata_lock[2], pdata_r[1];
//    //uint16_t adcResult_pot, adcResult_sensor, adc_samples[100];
//    uint8_t lock_address = 0x01, data_address = 0x10;
//    uint16_t slave_address = 0x48;
//    I2C2_MESSAGE_STATUS pstatus = 0;
//    
//    
//
//    /* Call the System Initialize routine*/
//    SYS_Initialize ( );
////    SYSTEM_Initialize( );
//    PIN_MANAGER_Initialize();
//    INTERRUPT_Initialize();
//    I2C2_Initialize();
//    
//    /* To determine how the LED and Buttons are mapped to the actual board
//     * features, please see io_mapping.h. */
//    LED_Enable ( LED_BLINK_ALIVE );
//    LED_Enable ( LED_BUTTON_PRESSED );
//
//    BUTTON_Enable ( BUTTON_DEMO );
//    static I2C2_TRANSACTION_REQUEST_BLOCK   trBlock[2];
////    static I2C2_TRANSACTION_REQUEST_BLOCK   rBlock;
//    while(1)
//    {
//        
//        pdata_lock[0] = lock_address;
//        pdata_lock[1] = 0x00;
//        pdata_w[0] = data_address;
//        pdata_w[1] = 0x1F;
//        pdata_r[0] = 0x00;
////        pdata_r[1] = 0x02;
//        for(j=0; j<100; j++)
//        {
//            I2C2_MasterWrite(pdata_lock, 2, slave_address, &pstatus);
//            while(!I2C2_MasterQueueIsEmpty());
//            I2C2_MasterWrite(pdata_w, 2, slave_address, &pstatus);
//            while(!I2C2_MasterQueueIsEmpty());
//            
//            I2C2_MasterWriteTRBBuild(&trBlock[0], pdata_w, 1, slave_address);
//            I2C2_MasterReadTRBBuild(&trBlock[1], pdata_r, 1, slave_address);
//            I2C2_MasterTRBInsert(2, &trBlock[0], &pstatus);
////            if(pstatus != I2C2_MESSAGE_COMPLETE)
////            {
////                I2C2_MasterTRBInsert(1, &rBlock, &pstatus);
////            }
//            
////            I2C2_MasterRead(pdata_r, 1, address, &pstatus);
//            while(pstatus != I2C2_MESSAGE_COMPLETE);
//        }
//            j = 0;
//            while(j < 2000)
//            {
//                j+=1;
//            }
//    }
//    
//    /* Get a timer event once every 100ms for the blink alive. */
//    TIMER_SetConfiguration ( TIMER_CONFIGURATION_1MS );
//    TIMER_RequestTick( &TimerEventHandler, 100 );
//    
//    /* The TIMER_1MS configuration should come before the RTCC initialization as
//     * there are some processor modules that require the TIMER_1MS module to be
//     * configured before the RTCC module, as the RTCC module is emulated using
//     * the TIMER_1MS module. */
//    time.bcdFormat = false;
//    RTCC_BuildTimeGet( &time );
//    RTCC_Initialize( &time );
//    ADC_SetConfiguration ( ADC_CONFIGURATION_DEFAULT );
//    //ADC_ChannelEnable ( ADC_CHANNEL_POTENTIOMETER );
//    ADC_ChannelEnable ( ADC_CHANNEL_RICHA_SENSOR );
//    /* Clear the screen */
//    
////    printf("outside\n");
//    printf( "\f" );   
//    while(1)
//    {
//        for(j = 0; j < 100; j++)
//        {
//            //adcResult_pot = ADC_Read10bit( ADC_CHANNEL_POTENTIOMETER );
//            adc_samples[j] = ADC_Read10bit( ADC_CHANNEL_RICHA_SENSOR );
//            avg_adc += (adc_samples[j] / 100);
//            RTCC_TimeGet( &time );
//
//            /*printf( "Time %02d:%02d:%02d   Pot = %4d\r\n",*/
//
//
//            /* To determine how the LED and Buttons are mapped to the actual board
//             * features, please see io_mapping.h. */
////            if(BUTTON_IsPressed( BUTTON_DEMO ) == true)
////            {
////                LED_On( LED_BUTTON_PRESSED );
////                printf( "Ss = %4d\r\n",
////                    //adcResult_pot,
////                    adc_samples[j]
////                  );
////            }
////            else
////            {
////                LED_Off( LED_BUTTON_PRESSED );
////            }
//        }
//        avg_adc = (avg_adc * 3.3) / 1023;
//    }
//}
//
//static void TimerEventHandler(void)
//{    
//    LED_Toggle( LED_BLINK_ALIVE );
//}
