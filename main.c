/*******************************************************************************
Copyright 2016 Microchip Technology Inc. (www.microchip.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <p24FJ256GA705.h>

#include "bsp/adc.h"
#include "bsp/lcd.h"
#include "bsp/timer_1ms.h"
#include "bsp/buttons.h"
#include "bsp/leds.h"
#include "bsp/rtcc.h"
 
#include "io_mapping.h"

#include "mcc_generated_files/mcc.h"

// *****************************************************************************
// *****************************************************************************
// Section: File Scope Variables and Functions
// *****************************************************************************
// *****************************************************************************
extern void SYS_Initialize ( void ) ;
static void TimerEventHandler( void );

static RTCC_DATETIME time;

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************
int main ( void )
{
    float avg_adc = 0;
    int j, i;
    uint8_t pdata_tia[2], pdata_ref[2], pdata_mode[2], pdata_lock[2], pdata_r1[1], pdata_r2[1], pdata_r3[1];
    //uint16_t adcResult_pot, adcResult_sensor, adc_samples[100];
    uint8_t lock_address = 0x01, tiacn_address = 0x10, refcn_address = 0x11, modecn_address = 0x12;
    uint16_t slave_address = 0x48;
    I2C2_MESSAGE_STATUS pstatus = 0;
    static I2C2_TRANSACTION_REQUEST_BLOCK   trBlock[2];

    /* Call the System Initialize routine*/
//    SYS_Initialize ( );
    PIN_MANAGER_Initialize();
    INTERRUPT_Initialize();
    I2C2_Initialize();
    LATBbits.LATB7 = 0;
//    TRISBbits.TRISB7 = 0;
//    TRISBbits.TRISB8 = 1;
    UART2_Initialize();
    
        
    /* enable Leds */
    LED_Enable ( LED_BLINK_ALIVE );
    LED_Enable ( LED_BUTTON_PRESSED );
    
    /* enable button */
    BUTTON_Enable ( BUTTON_DEMO );

    /* configure the lmp91000 LOCK register */
    pdata_lock[0] = lock_address;
    pdata_lock[1] = 0x00;
    I2C2_MasterWrite(pdata_lock, 2, slave_address, &pstatus);
    while(!I2C2_MasterQueueIsEmpty());
    while(pstatus != I2C2_MESSAGE_COMPLETE);
    
    /* configure lmp91000 TIACN register */
    pdata_tia[0] = tiacn_address;
    pdata_tia[1] = 0x1C;
    I2C2_MasterWrite(pdata_tia, 2, slave_address, &pstatus);
    while(!I2C2_MasterQueueIsEmpty());
    while(pstatus != I2C2_MESSAGE_COMPLETE);
    
    /* configure lmp91000 REFCN register */
    pdata_ref[0] = refcn_address;
    pdata_ref[1] = 0x14;
    I2C2_MasterWrite(pdata_ref, 2, slave_address, &pstatus);
    while(!I2C2_MasterQueueIsEmpty());
    while(pstatus != I2C2_MESSAGE_COMPLETE);
    
    /* configure lmp91000 MODECN register */
    pdata_mode[0] = modecn_address;
    pdata_mode[1] = 0x03;
    I2C2_MasterWrite(pdata_mode, 2, slave_address, &pstatus);
    while(!I2C2_MasterQueueIsEmpty());
    while(pstatus != I2C2_MESSAGE_COMPLETE);
    
    /* read the registers */
    pdata_r1[0] = 0x00;
    pdata_r2[0] = 0x00;
    pdata_r3[0] = 0x00;
    
    I2C2_MasterWriteTRBBuild(&trBlock[0], pdata_tia, 1, slave_address);
    I2C2_MasterReadTRBBuild(&trBlock[1], pdata_r1, 1, slave_address);
    I2C2_MasterTRBInsert(2, &trBlock[0], &pstatus);
    while(pstatus != I2C2_MESSAGE_COMPLETE);
    
    I2C2_MasterWriteTRBBuild(&trBlock[0], pdata_ref, 1, slave_address);
    I2C2_MasterReadTRBBuild(&trBlock[1], pdata_r2, 1, slave_address);
    I2C2_MasterTRBInsert(2, &trBlock[0], &pstatus);
    while(pstatus != I2C2_MESSAGE_COMPLETE);
    
    I2C2_MasterWriteTRBBuild(&trBlock[0], pdata_mode, 1, slave_address);
    I2C2_MasterReadTRBBuild(&trBlock[1], pdata_r3, 1, slave_address);
    I2C2_MasterTRBInsert(2, &trBlock[0], &pstatus);
    while(pstatus != I2C2_MESSAGE_COMPLETE);
    
    UART2_Write(pdata_r1[0]);
    while(!(UART2_StatusGet() & UART2_TX_COMPLETE));  
    UART2_Write(pdata_r2[0]);
    while(!(UART2_StatusGet() & UART2_TX_COMPLETE));  
    UART2_Write(pdata_r3[0]);
    while(!(UART2_StatusGet() & UART2_TX_COMPLETE));  
    
//    /* Get a timer event once every 100ms for the blink alive. */
//    TIMER_SetConfiguration ( TIMER_CONFIGURATION_1MS );
//    TIMER_RequestTick( &TimerEventHandler, 100 );
//    
//    /* The TIMER_1MS configuration should come before the RTCC initialization as
//     * there are some processor modules that require the TIMER_1MS module to be
//     * configured before the RTCC module, as the RTCC module is emulated using
//     * the TIMER_1MS module. */
//    time.bcdFormat = false;
//    RTCC_BuildTimeGet( &time );
//    RTCC_Initialize( &time );
//    ADC_SetConfiguration ( ADC_CONFIGURATION_DEFAULT );
//    //ADC_ChannelEnable ( ADC_CHANNEL_POTENTIOMETER );
//    ADC_ChannelEnable ( ADC_CHANNEL_RICHA_SENSOR );
//    /* Clear the screen */
//    
////    printf("outside\n");
//    printf( "\f" );   
//    while(1)
//    {
//        for(j = 0; j < 100; j++)
//        {
//            //adcResult_pot = ADC_Read10bit( ADC_CHANNEL_POTENTIOMETER );
//            adc_samples[j] = ADC_Read10bit( ADC_CHANNEL_RICHA_SENSOR );
//            avg_adc += (adc_samples[j] / 100);
//            RTCC_TimeGet( &time );
//
//            /*printf( "Time %02d:%02d:%02d   Pot = %4d\r\n",*/
//
//
//            /* To determine how the LED and Buttons are mapped to the actual board
//             * features, please see io_mapping.h. */
////            if(BUTTON_IsPressed( BUTTON_DEMO ) == true)
////            {
////                LED_On( LED_BUTTON_PRESSED );
////                printf( "Ss = %4d\r\n",
////                    //adcResult_pot,
////                    adc_samples[j]
////                  );
////            }
////            else
////            {
////                LED_Off( LED_BUTTON_PRESSED );
////            }
//        }
//        avg_adc = (avg_adc * 3.3) / 1023;
//    }
}

static void TimerEventHandler(void)
{    
    LED_Toggle( LED_BLINK_ALIVE );
}
